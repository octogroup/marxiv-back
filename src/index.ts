import { copy, writeFile } from 'fs-extra';

// Import and re-export our SASS -> CSS builder.
import { writeCSSFiles } from './compileSass';
export { writeCSSFiles };

import { HastRoot, HastBody, HastHead } from '@octogroup/hast-typescript';
// Hast to HTML
const toHtml = require('hast-util-to-html');

/**
 * Shared Imports
 */
import { MarXivData, MarXivDataWithoutTranslatables, PageMetadata, WorkMetadata, MarXivDataWithoutSitemap, } from '@octogroup/marxiv-common';
import { englishPageMetadata, Translatables, } from '@octogroup/marxiv-common';
import { makeMarXivSitemap, makeMinimalMarXivSitemap } from '@octogroup/marxiv-common';
import { makePage, buildHastRoot, companionFns, staticAssets } from '@octogroup/marxiv-common';
import { supplyTranslatables, TranslatableMetadata, SupportedLanguage } from '@octogroup/marxiv-common';
import { SubmenuType, UserMenuType, } from '@octogroup/marxiv-common';
/**
 * Import our testing data
 */
import { dummyInstitutions, dummyPeerReview, dummyRefs, dummyRelatedMaterials, dummyPaginationDataArray, dummyUserData, dummyWork, dummyWorksForListing, testingActiveWork, } from '@octogroup/marxiv-common/dist/testingData';
import { WorkData } from '@octogroup/marxiv-common/dist/works';

/**
 * Build a minimal sitemap to generate the ChangeURLCB
 */
export const englishMarXivMinimalSitemap = makeMinimalMarXivSitemap('en')(dummyWorksForListing)(englishPageMetadata);

/**
 * EVERYTHING to build a page MUST be in the context at this point, or we cannot build the Sitemap.
 */
const makeUniqueDummyWork = (template: WorkData) => (i: number): WorkData => ({
   ...template,
   workID: i.toString()
});

const makeDummyWorks = (template: WorkData) => (num: number): WorkData[] => {
   let dummyWorks: WorkData[] = new Array;
   for (let i = 0; i < num; i++) {
      dummyWorks.push( makeUniqueDummyWork(template)(i) );
   }
   return dummyWorks
}

export const testingContextMinimal = {
   requestedPath: '/submit/preprint',
   requestedLanguage: 'en' as SupportedLanguage,
   // Menus
   submenuToDisplay: 'none' as SubmenuType,
   userMenuToDisplay: 'anonymous' as UserMenuType,
   // Pagination
   paginationData: {
      currentPage: 1,
      hasNextPage: true,
      hasPreviousPage: false
   },
   paginationCBs: {
      nextPageCB: (event: Event) => {},
      previousPageCB: (event: Event) => {},
   },
   gtag: {},
   // User data
   userData: dummyUserData,
   // Active Work Data
   workData: testingActiveWork,
   // Work listing
   works: makeDummyWorks(dummyWork)(14),
   // The user's works
   userWorks: dummyWorksForListing,
}

/**
 * Testing context without translatables
 */
export const testingContextWithoutTranslatables: MarXivDataWithoutSitemap = {
   minimalSitemap: englishMarXivMinimalSitemap,
   assets: staticAssets,
   currentPage: {
      title: '',
      localURL: '',
      depth: 'primary'
   },
   ...companionFns,
   ...testingContextMinimal,
};

/**
 * Make the English-language translatables
 */
export const translatablesEnglish: TranslatableMetadata = supplyTranslatables(testingContextWithoutTranslatables)('en');

/**
 * Make the English-language sitemap
 */
export const sitemapEnglish = makeMarXivSitemap(testingContextWithoutTranslatables)(translatablesEnglish.translatables);

/**
 * Assemble the Context
 */
export const testingContext: MarXivData = {
   ... testingContextWithoutTranslatables,
   ...translatablesEnglish,
   sitemap: sitemapEnglish,
};

/**
 * Try to build directories
 */
const exec = require('child_process').exec;

const _writeHTMLFile = (langCode: SupportedLanguage) => (path: string) => (hastHTML: HastRoot) => writeFile(path === '/' ? './dist/html/' + langCode + '/index.html' : './dist/html/' + langCode + path + '/index.html', toHtml(hastHTML), err => {
   if (!err) {
     //file written on disk
  }
  else {
     console.log("Error writing file.");
     console.log(err);
  }
});

const _makeDirectory = (langCode: SupportedLanguage) => (path: string) => (hastHTML: HastRoot) => exec('mkdir -p ' + './dist/html/' + langCode + path, (err: any, stdout: any, stderr: any) => {
   if (!err && !stderr) {
      // Success
      _writeHTMLFile(langCode)(path)(hastHTML);
   }
   else {
      console.log(err);
      console.log(stderr);
   }
});

const _makeSimpleDirectory = (path: string) => exec('mkdir -p ' + './dist/' + path, (err: any, stdout: any, stderr: any) => {
   if (!err && !stderr) {
      // Success
   }
   else {
      console.log(err);
      console.log(stderr);
   }
});

const _copyAsset = (source: string, output: string) => copy(source, output, (err: any) => {
   if (err) {
      console.log(err)
   }
});

/**
 * Write an HTML file to the disk.
 */
export const makeWorkPageFromSitemap = (context: MarXivDataWithoutTranslatables) => (d: Translatables) => (reqPath: string): { head: HastHead, body: HastBody } => {
   // Loop over the workMetadata and match on local paths
   let pageFound: WorkMetadata[] = new Array;
   for (let i = 0; i < context.sitemap.works.works.length; i++) {
      let work = context.sitemap.works.works[i];
      if (work.localURL === reqPath) {
         pageFound.push(work);
      }
   }
   // If we didn't find the page, throw an error
   if (pageFound.length === 0) {
      console.log(reqPath);
      throw new Error('Requested Work page does not exist. The requested path was: ' + reqPath)
   }
   else if (pageFound.length === 1) {
      return makePage(context)(d)(pageFound[0]);
   }
   else {
      throw new Error('Requested path matched on more than one Work localURL.')
   }
}

export const makeWorksPaginatedPageFromSitemap = (context: MarXivDataWithoutTranslatables) => (d: Translatables) => (reqPath: string): { head: HastHead, body: HastBody } => {
   // Loop over the PageMetadata and match on local paths
   let pageFound: PageMetadata[] = new Array;
   for (let i = 0; i < context.sitemap.works.paginatedWorksPages.length; i++) {
      let paginatedPage = context.sitemap.works.paginatedWorksPages[i];
      if (paginatedPage.localURL === reqPath) {
         pageFound.push(paginatedPage);
      }
   }
   // If we didn't find the page, throw an error
   if (pageFound.length === 0) {
      console.log(reqPath);
      throw new Error('Requested Paginated Work page does not exist. The requested path was: ' + reqPath)
   }
   else if (pageFound.length === 1) {
      return makePage(context)(d)(pageFound[0]);
   }
   else {
      throw new Error('Requested path matched on more than one Paginated Work localURL.')
   }
}

/**
 * Determine if the reqPath is for a Works page or a paginated Works page, or if the path doesn't exist.
 */
const dispatchWorkOrPaginated = (context: MarXivDataWithoutTranslatables) => (d: Translatables) =>  (reqPath: string): { head: HastHead, body: HastBody } => {
   if (reqPath.startsWith('/page/')) {
      return makeWorksPaginatedPageFromSitemap(context)(d)(reqPath);
   }
   else {
      return makeWorkPageFromSitemap(context)(d)(reqPath);
   }
}

export const makePageFromSitemap = (reqPath: string) => (context: MarXivDataWithoutTranslatables) => (d: Translatables): { head: HastHead, body: HastBody } => {
   switch (reqPath) {
      // Homepage
      case context.sitemap.homepage.localURL:
         return makePage(context)(d)(context.sitemap.homepage);
      // Work Listing
      case context.sitemap.works.workListing.localURL:
         return makePage(context)(d)(context.sitemap.works.workListing);
      // Why MarXiv
      case context.sitemap.why.whyMarXiv.localURL:
         return makePage(context)(d)(context.sitemap.why.whyMarXiv);
      // MarXiv Team
      case context.sitemap.why.team.localURL:
         return makePage(context)(d)(context.sitemap.why.team);
      // MarXiv Ambassadors
      case context.sitemap.why.ambassadors.localURL:
         return makePage(context)(d)(context.sitemap.why.ambassadors);
      // MarXiv for Orgs
      case context.sitemap.forOrgs.localURL:
         return makePage(context)(d)(context.sitemap.forOrgs);
      // Privacy Policy
      case context.sitemap.privacyPolicy.localURL:
         return makePage(context)(d)(context.sitemap.privacyPolicy);
      // Celebrate
      case context.sitemap.celebrate.localURL:
         return makePage(context)(d)(context.sitemap.celebrate);
      // License Options
      case context.sitemap.licenses.localURL:
         return makePage(context)(d)(context.sitemap.licenses);
      // Work Metadata (types of works accepted)
      case context.sitemap.worksAccepted.localURL:
         return makePage(context)(d)(context.sitemap.worksAccepted);
      // Code of Conduct
      case context.sitemap.share.codeOfConduct.localURL:
         return makePage(context)(d)(context.sitemap.share.codeOfConduct);
      // Self-archiving Policies
      case context.sitemap.share.selfArchivingPolicies.localURL:
         return makePage(context)(d)(context.sitemap.share.selfArchivingPolicies);
      // Submission Guidelines
      case context.sitemap.share.submissionGuidelines.localURL:
         return makePage(context)(d)(context.sitemap.share.submissionGuidelines);
      // Google Custom Search
      case context.sitemap.search.localURL:
         return makePage(context)(d)(context.sitemap.search);
      // Join
      case context.sitemap.join.localURL:
         return makePage(context)(d)(context.sitemap.join);
      // Login
      case context.sitemap.login.localURL:
         return makePage(context)(d)(context.sitemap.login);
      // Logout
      case context.sitemap.logout.localURL:
         return makePage(context)(d)(context.sitemap.logout);
      // Reset password
      case context.sitemap.resetPassword.localURL:
         return makePage(context)(d)(context.sitemap.resetPassword);
      // Admin dashboard
      case context.sitemap.admin.localURL:
         return makePage(context)(d)(context.sitemap.admin);
      // User dashboard
      case context.sitemap.user.dashboard.localURL:
         return makePage(context)(d)(context.sitemap.user.dashboard);
      // User account
      case context.sitemap.user.account.localURL:
         return makePage(context)(d)(context.sitemap.user.account);
      // Edit user account
      case context.sitemap.user.editAccount.localURL:
         return makePage(context)(d)(context.sitemap.user.editAccount);
      // Admin edit user account
      case context.sitemap.adminEditAccount.localURL:
         return makePage(context)(d)(context.sitemap.adminEditAccount);
      // Confirm New Submission
      case context.sitemap.confirm.newSubmission.localURL:
         return makePage(context)(d)(context.sitemap.confirm.newSubmission);
      // Confirm Updated Work
      case context.sitemap.confirm.updatedWork.localURL:
         return makePage(context)(d)(context.sitemap.confirm.updatedWork);
      // Confirm Resubmit
      case context.sitemap.confirm.resubmit.localURL:
         return makePage(context)(d)(context.sitemap.confirm.resubmit);
      // Confirm New Account
      case context.sitemap.confirm.newAccount.localURL:
         return makePage(context)(d)(context.sitemap.confirm.newAccount);
      // Confirm Verify Email
      case context.sitemap.confirm.verifyEmail.localURL:
         return makePage(context)(d)(context.sitemap.confirm.verifyEmail);
      // Share Splash PAge
      case context.sitemap.share.splash.localURL:
         return makePage(context)(d)(context.sitemap.share.splash);
      // Submission Guide - Page 1
      case context.sitemap.share.submissionGuide1.localURL:
         return makePage(context)(d)(context.sitemap.share.submissionGuide1);
      // Submission Guide - Published Options
      case context.sitemap.share.submissionGuidePublishedOptions.localURL:
         return makePage(context)(d)(context.sitemap.share.submissionGuidePublishedOptions);
      // Submission Guide - Published & Copyrighted Options
      case context.sitemap.share.submissionGuidePublishedCopyrighted.localURL:
         return makePage(context)(d)(context.sitemap.share.submissionGuidePublishedCopyrighted);
      // Submission Guide - Page 2
      case context.sitemap.share.submissionGuide2.localURL:
         return makePage(context)(d)(context.sitemap.share.submissionGuide2);
      // Submission Guide - Report Uptions
      case context.sitemap.share.submissionGuideReportOptions.localURL:
         return makePage(context)(d)(context.sitemap.share.submissionGuideReportOptions);
      // Submission Guide - Conference Options
      case context.sitemap.share.submissionGuideConferenceOptions.localURL:
         return makePage(context)(d)(context.sitemap.share.submissionGuideConferenceOptions);
      // Submission Guide - Dataset & Database Options
      // case context.sitemap.share.submissionGuideDatasetOptions.localURL:
      //    return makePage(context)(d)(context.sitemap.share.submissionGuideDatasetOptions);
      // Submission Guide - Page 3
      case context.sitemap.share.submissionGuide3.localURL:
         return makePage(context)(d)(context.sitemap.share.submissionGuide3);
      // ------------------------
      // Upload Works - Preprint
      case context.sitemap.submit.preprint.localURL:
         return makePage(context)(d)(context.sitemap.submit.preprint);
      // Upload Works - Postprint
      case context.sitemap.submit.postprint.localURL:
         return makePage(context)(d)(context.sitemap.submit.postprint);
      // Upload Works - Open Access
      case context.sitemap.submit.openAccess.localURL:
         return makePage(context)(d)(context.sitemap.submit.openAccess);
      // Upload Works - Un-Copyrighted
      case context.sitemap.submit.unCopyrighted.localURL:
         return makePage(context)(d)(context.sitemap.submit.unCopyrighted);
      // Upload Works - Unpublished Report
      case context.sitemap.submit.unpublishedReport.localURL:
         return makePage(context)(d)(context.sitemap.submit.unpublishedReport);
      // Upload Works - Thesis
      case context.sitemap.submit.thesis.localURL:
         return makePage(context)(d)(context.sitemap.submit.thesis);
      // Upload Works - Database
      // case context.sitemap.submit.database.localURL:
      //    return makePage(context)(d)(context.sitemap.submit.database);
      // Upload Works - Dataset
      // case context.sitemap.submit.dataset.localURL:
      //    return makePage(context)(d)(context.sitemap.submit.dataset);
      // Upload Works - Poster, etc.
      case context.sitemap.submit.posterPresentationSI.localURL:
         return makePage(context)(d)(context.sitemap.submit.posterPresentationSI);
      // Upload Works - Letter
      case context.sitemap.submit.letter.localURL:
         return makePage(context)(d)(context.sitemap.submit.letter);
      // Upload Works - Working Paper
      case context.sitemap.submit.workingPaper.localURL:
         return makePage(context)(d)(context.sitemap.submit.workingPaper);
      // Upload Works - Peer Review
      // case context.sitemap.submit.peerReview.localURL:
      //    return makePage(context)(d)(context.sitemap.submit.peerReview);
      // Upload Works - Published Report
      // case context.sitemap.submit.publishedReport.localURL:
      //    return makePage(context)(d)(context.sitemap.submit.publishedReport);
      // Upload Works - Published Conference Paper
      // case context.sitemap.submit.publishedConferencePaper.localURL:
      //    return makePage(context)(d)(context.sitemap.submit.publishedConferencePaper);
      // --------------
      // Edit Works - Preprint
      case context.sitemap.edit.preprint.localURL:
         return makePage(context)(d)(context.sitemap.edit.preprint);
      // Edit Works - Postprint
      case context.sitemap.edit.postprint.localURL:
         return makePage(context)(d)(context.sitemap.edit.postprint);
      // Edit Works - Open Access
      case context.sitemap.edit.openAccess.localURL:
         return makePage(context)(d)(context.sitemap.edit.openAccess);
      // Edit Works - Un-Copyrighted
      case context.sitemap.edit.unCopyrighted.localURL:
         return makePage(context)(d)(context.sitemap.edit.unCopyrighted);
      // Edit Works - Unpublished Report
      case context.sitemap.edit.unpublishedReport.localURL:
         return makePage(context)(d)(context.sitemap.edit.unpublishedReport);
      // Edit Works - Thesis
      case context.sitemap.edit.thesis.localURL:
         return makePage(context)(d)(context.sitemap.edit.thesis);
      // Edit Works - Database
      // case context.sitemap.edit.database.localURL:
      //    return makePage(context)(d)(context.sitemap.edit.database);
      // Edit Works - Dataset
      // case context.sitemap.edit.dataset.localURL:
      //    return makePage(context)(d)(context.sitemap.edit.dataset);
      // Edit Works - Poster, etc.
      case context.sitemap.edit.posterPresentationSI.localURL:
         return makePage(context)(d)(context.sitemap.edit.posterPresentationSI);
      // Edit Works - Letter
      case context.sitemap.edit.letter.localURL:
         return makePage(context)(d)(context.sitemap.edit.letter);
      // Edit Works - Working Paper
      case context.sitemap.edit.workingPaper.localURL:
         return makePage(context)(d)(context.sitemap.edit.workingPaper);
      // Edit Works - Peer Review
      // case context.sitemap.edit.peerReview.localURL:
      //    return makePage(context)(d)(context.sitemap.edit.peerReview);
      // Edit Works - Published Report
      // case context.sitemap.edit.publishedReport.localURL:
      //    return makePage(context)(d)(context.sitemap.edit.publishedReport);
      // Edit Works - Published Conference Paper
      // case context.sitemap.edit.publishedConferencePaper.localURL:
      //    return makePage(context)(d)(context.sitemap.edit.publishedConferencePaper);
      // --------------
      // Moderate Works - Preprint
      case context.sitemap.moderate.preprint.localURL:
         return makePage(context)(d)(context.sitemap.moderate.preprint);
      // Moderate Works - Postprint
      case context.sitemap.moderate.postprint.localURL:
         return makePage(context)(d)(context.sitemap.moderate.postprint);
      // Moderate Works - Open Access
      case context.sitemap.moderate.openAccess.localURL:
         return makePage(context)(d)(context.sitemap.moderate.openAccess);
      // Moderate Works - Un-Copyrighted
      case context.sitemap.moderate.unCopyrighted.localURL:
         return makePage(context)(d)(context.sitemap.moderate.unCopyrighted);
      // Moderate Works - Unpublished Report
      case context.sitemap.moderate.unpublishedReport.localURL:
         return makePage(context)(d)(context.sitemap.moderate.unpublishedReport);
      // Moderate Works - Thesis
      case context.sitemap.moderate.thesis.localURL:
         return makePage(context)(d)(context.sitemap.moderate.thesis);
      // Moderate Works - Database
      // case context.sitemap.moderate.database.localURL:
      //    return makePage(context)(d)(context.sitemap.moderate.database);
      // Moderate Works - Dataset
      // case context.sitemap.moderate.dataset.localURL:
      //    return makePage(context)(d)(context.sitemap.moderate.dataset);
      // Moderate Works - Poster, etc.
      case context.sitemap.moderate.posterPresentationSI.localURL:
         return makePage(context)(d)(context.sitemap.moderate.posterPresentationSI);
      // Moderate Works - Letter
      case context.sitemap.moderate.letter.localURL:
         return makePage(context)(d)(context.sitemap.moderate.letter);
      // Moderate Works - Working Paper
      case context.sitemap.moderate.workingPaper.localURL:
         return makePage(context)(d)(context.sitemap.moderate.workingPaper);
      // Moderate Works - Peer Review
      // case context.sitemap.moderate.peerReview.localURL:
      //    return makePage(context)(d)(context.sitemap.moderate.peerReview);
      // Moderate Works - Published Report
      // case context.sitemap.moderate.publishedReport.localURL:
      //    return makePage(context)(d)(context.sitemap.moderate.publishedReport);
      // Moderate Works - Published Conference Paper
      // case context.sitemap.moderate.publishedConferencePaper.localURL:
      //    return makePage(context)(d)(context.sitemap.moderate.publishedConferencePaper);
      // --- Error pages
      case context.sitemap.error.accessDenied.localURL:
         return makePage(context)(d)(context.sitemap.error.accessDenied);
      case context.sitemap.error.notFound.localURL:
         return makePage(context)(d)(context.sitemap.error.notFound);
      case context.sitemap.error.uhoh.localURL:
         return makePage(context)(d)(context.sitemap.error.uhoh);
      // At this point, the requested path:
      // a) is a Work page,
      // b) is a Paginated Work page, or
      // c) doens't exist.
      default:
         return dispatchWorkOrPaginated(context)(d)(reqPath);
   }
}

export const generateHTMLPage = (context: MarXivDataWithoutTranslatables) => (translatables: TranslatableMetadata) => (reqPath: string): HastRoot => buildHastRoot( makePageFromSitemap(reqPath)(context)(translatables.translatables) )(translatables.code);

const writeHTMLFiles = (context: MarXivData): void => Object.entries(context.sitemap).forEach((value) => {
   if (typeof(value[1]) === 'object') {
      if (value[0] === 'works') {
         // This is a parent for nested pages.
         Object.entries(value[1]).forEach((subValue: [string, any]) => {
            if (subValue[0] === 'workListing') {
               let subpageDataWorkListing: PageMetadata = subValue[1];
               let subPathWorkListing = subpageDataWorkListing.localURL;
               return _makeDirectory(context.code)('/works')( generateHTMLPage({ ...context, currentPage: { title: subpageDataWorkListing.title, localURL: subpageDataWorkListing.localURL, depth: subpageDataWorkListing.depth } })({ translatables: context.translatables, code: context.requestedLanguage })(subPathWorkListing) );
            }
            else if (subValue[0] === 'works') {
               // This is an array of WorkMetadata
               let workMetadataArray: WorkMetadata[] = subValue[1];
               workMetadataArray.forEach((workPage) => {
                  // Add the language prefix
                  let workPath = workPage.localURL;
                  return _makeDirectory(context.code)(workPath)( generateHTMLPage({ ...context, currentPage: { title: workPage.title, localURL: workPage.localURL, depth: workPage.depth } })({ translatables: context.translatables, code: context.requestedLanguage })(workPath) );
               })
            }
            else if (subValue[0] === 'paginatedWorksPages') {
               // This is an array of PageMetadata
               let subpageDataPaginatedWorksArray: PageMetadata[] = subValue[1];
               subpageDataPaginatedWorksArray.forEach((paginatedPage) => {
                  // Add the language prefix
                  let paginatedPath = paginatedPage.localURL;
                  return _makeDirectory(context.code)(paginatedPath)( generateHTMLPage({ ...context, currentPage: { title: paginatedPage.title, localURL: paginatedPage.localURL, depth: paginatedPage.depth } })({ translatables: context.translatables, code: context.requestedLanguage })(paginatedPath) );
               })
            }
            else {
               throw new Error('Encountered unexpected Works child object.')
            }
         });
      }
      else {
         if (value[1].bodyContent) {
            // This is a single page.
            let data: PageMetadata = value[1];
            // Run generateHTMLPage with the localURL
            let path = data.localURL;
            return _makeDirectory(context.code)(path)( generateHTMLPage({ ...context, currentPage: { title: data.title, localURL: data.localURL, depth: data.depth } })({ translatables: context.translatables, code: context.requestedLanguage })(path) );
         }
         else {
            // This is a parent for nested pages.
            Object.entries(value[1]).forEach((subValue: [string, any]) => {
               if (subValue[1].bodyContent) {
                  // Since this is the parent for a folder, the Parent page needs to be saved as /parent/index.html
                  let subpageData: PageMetadata = subValue[1];
                  let subPath = subpageData.localURL;
                  return _makeDirectory(context.code)(subPath)( generateHTMLPage({ ...context, currentPage: { title: subpageData.title, localURL: subpageData.localURL, depth: subpageData.depth } })({ translatables: context.translatables, code: context.requestedLanguage })(subPath) );
               }
               else if (Array.isArray(value[1])) {
                  // This should only happen in Works pages.
                  throw new Error('Value is an array outside of Works.');
               }
               else {
                  throw new Error('Value is not an object or an array.');
               }
            })
         }
      }
   }
   else if (Array.isArray(value[1])) {
      // This should only happen in Works pages.
      throw new Error('Value is an array outside of Works.');
   }
   else {
      throw new Error('Value is not an object or an array.');
   }
});

/**
 * Export a function to build the backend scripts from Context.
 * Generates files in `dist/html/`
 */
export const writeBackendFiles = (context?: MarXivData): void => {
   if (context) {
      return writeHTMLFiles(context);
   }
   else {
      return writeHTMLFiles(testingContext);
   }
}


/**
 * Build our environment in `dist/html`
 */

// Make our directories.
_makeSimpleDirectory('html');
_makeSimpleDirectory('html/css');
_makeSimpleDirectory('html/js');
_makeSimpleDirectory('html/images');
_makeSimpleDirectory('html/pdf');

// Convert SCSS --> CSS
writeCSSFiles();

// Copy over static assets.
_copyAsset('./src/pdf', './dist/html/pdf');
_copyAsset('./src/images', './dist/html/images');

// Copy over React bundles.
_copyAsset('./node_modules/@octogroup/marxiv-front/dist/bundle', './dist/html/js');

// Copy over JS scripts.
_copyAsset('./node_modules/@octogroup/marxiv-front/dist/scripts', './dist/html/js');

// Write all the backend HTML files
writeHTMLFiles(testingContext);