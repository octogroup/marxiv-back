/**
 * We need to compile our SCSS (aka SASS) into CSS for the browser.
 */

import * as sass from 'node-sass';
import * as fs from 'fs';

const writeSass = (inFile: string, outFile: string): void => {
   sass.render({
      file: inFile,
      outputStyle: 'nested',
      outFile: outFile,
      // We should enable source maps for debugging, so we can see the actual SCSS rather than compiled CSS.
      sourceMap: true,
      sourceMapContents: true,
      sourceMapEmbed: true,
   }, (err, result) => {
      if (!err) {
         // Write the compiled CSS file.
         fs.writeFile(outFile, result.css, (err) => {
            if (!err) {
               //file written on disk
            } else {
               console.log("Error writing file.");
            }
         });
      } else {
         console.log("Error compiling CSS from SCSS.");
         console.log(err);
      }
   });
};

/**
 * Convert SCSS --> CSS
 * 
 * @param inFile './src/scss/style.scss'
 * @param outFile './dist/html/css/style.css'
 */
export const writeCSSFiles = (inFile?: string, outFile?: string): void => {
   if (inFile && outFile) {
      return writeSass(inFile, outFile);
   }
   else if (inFile) {
      const outFile = './dist/html/css/style.css';
      return writeSass(inFile, outFile);
   }
   else if (outFile) {
      const inFile = './src/scss/style.scss';
      return writeSass(inFile, outFile);
   }
   else {
      const inFile = './src/scss/style.scss';
      const outFile = './dist/html/css/style.css';
      return writeSass(inFile, outFile);
   }
};